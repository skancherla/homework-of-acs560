package primegen;

public class PrimeGenerator {
  private static boolean[] crossedOut;
  private static int[] result;

  public static int[] generatePrimes(int maxValue) {
	  if (maxValue < 2) {
		  return new int[0];  
	  }
	  else {
		  uncrossIntegersUpTo(maxValue);
		  crossOutMultiples();
		  putUncrossedIntegersIntoResult();
		  return result;
		  
	  }
  }

  private static void uncrossIntegersUpTo(int maxValue) {
    crossedOut = new boolean[maxValue + 1];//assuming that maxValue numbers in array are all false by default, on length of maxValue + 1, but can't be more than that because index out of bounds
    
    
    
    for (int i = 2; i < crossedOut.length; i++) {
      crossedOut[i] = false;//do not cross out numbers array starting from length 2 to maxNumber
    }
    
    //but odd thing is that crossedOut[0] and crossedOut[1] are false by default
    
  }

  private static void crossOutMultiples() {
	  
	  
	  int limit = determineIterationLimit();
	  
	  //must have i < = limit
    for (int i = 2; i < limit; i++)
      if (notCrossed(i))
        crossOutMultiplesOf(i);
  }

  private static int determineIterationLimit()
  {
    // Every multiple in the array has a prime factor that
    // is less than or equal to the root of the array size,
    // so we don't have to cross of multiples of numbers
    // larger than that root.
    double iterationLimit = Math.sqrt(crossedOut.length);
    return (int) iterationLimit;
  }

  private static void crossOutMultiplesOf(int i)
  {
    for (int multiple = 2*i;
         multiple < crossedOut.length;
         multiple += i)
      crossedOut[multiple] = true;
  }

  private static boolean notCrossed(int i)
  {
    return crossedOut[i] == false;
  }

  
  private static void putUncrossedIntegersIntoResult()
  {
    result = new int[numberOfUncrossedIntegers()];
    for (int j = 0, i = 2; i < crossedOut.length; i++)
      if (notCrossed(i)) {
    	  result[j++] = i;
        //numbers are uncrossed means prime numbers in array
      }
    

  }

  //length of numbers that are not crossed which are prime numbers
  private static int numberOfUncrossedIntegers()
  {
    int count = 0;
    //must have i<=crossedOut.length, not the i < crossedOut.length..???
    for (int i = 2; i < crossedOut.length; i++)
      if (notCrossed(i))
        count++;
    
    return count;
  }
}