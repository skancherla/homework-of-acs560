﻿

using System;
using System.Windows.Forms;

namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        private void InitializeComponent()
        {
            //this.txtNum1 = new System.Windows.Forms.TextBox();
            //this.txtNum2 = new System.Windows.Forms.TextBox();

            this.txtResult = new System.Windows.Forms.TextBox();
            this.sevenButton = new System.Windows.Forms.Button();
            this.eightButton = new System.Windows.Forms.Button();
            this.nineButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.fourButton = new System.Windows.Forms.Button();
            this.fiveButton = new System.Windows.Forms.Button();
            this.sixButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.oneButton = new System.Windows.Forms.Button();
            this.twoButton = new System.Windows.Forms.Button();
            this.threeButton = new System.Windows.Forms.Button();
            this.subtractButton = new System.Windows.Forms.Button();
            this.zeroButton = new System.Windows.Forms.Button();
            this.decimalButton = new System.Windows.Forms.Button();
            this.equalButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();


            // 
            // txtNum1
            // 
            //this.txtNum1.Location = new System.Drawing.Point(143, 42);
            //this.txtNum1.Name = "txtNum1";
            //this.txtNum1.Size = new System.Drawing.Size(100, 20);
            //this.txtNum1.TabIndex = 2;
            //this.txtNum1.KeyPress += txtNum_KeyPress;
            // 
            // txtNum2
            // 
            //this.txtNum2.Location = new System.Drawing.Point(143, 84);
            //this.txtNum2.Name = "txtNum2";
            //this.txtNum2.Size = new System.Drawing.Size(100, 20);
            //this.txtNum2.TabIndex = 3;
            //this.txtNum2.KeyPress += txtNum_KeyPress;



            //
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(12, 12);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(249, 23);
            this.txtResult.TabIndex = 0;
            this.txtResult.Text = "0.";
            this.txtResult.TextAlign = HorizontalAlignment.Right;
            this.txtResult.ReadOnly = true;
            this.txtResult.Enabled = false;

            // 
            // button 7
            // 
            this.sevenButton.Location = new System.Drawing.Point(13, 60);
            this.sevenButton.Name = "sevenButton";
            this.sevenButton.Size = new System.Drawing.Size(56, 28);
            this.sevenButton.TabIndex = 1;
            this.sevenButton.Text = "7";
            this.sevenButton.UseVisualStyleBackColor = true;

            this.sevenButton.Click += new System.EventHandler(this.sevenButton_Click);//important to use click method so the form works with clicking button


            // 
            // button 8
            // 
            this.eightButton.Location = new System.Drawing.Point(75, 60);
            this.eightButton.Name = "eightButton";
            this.eightButton.Size = new System.Drawing.Size(56, 28);
            this.eightButton.TabIndex = 2;
            this.eightButton.Text = "8";
            this.eightButton.UseVisualStyleBackColor = true;

            this.eightButton.Click += new System.EventHandler(this.eightButton_Click);//important to use click method so the form works with clicking button

            // 
            // button 9
            // 
            this.nineButton.Location = new System.Drawing.Point(137, 60);
            this.nineButton.Name = "nineButton";
            this.nineButton.Size = new System.Drawing.Size(56, 28);
            this.nineButton.TabIndex = 3;
            this.nineButton.Text = "9";
            this.nineButton.UseVisualStyleBackColor = true;

            this.nineButton.Click += new System.EventHandler(this.nineButton_Click);//important to use click method so the form works with clicking button


            // 
            // division button
            // 
            this.divideButton.Location = new System.Drawing.Point(200, 60);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(56, 28);
            this.divideButton.TabIndex = 4;
            this.divideButton.Text = "/";
            this.divideButton.UseVisualStyleBackColor = true;

            this.divideButton.Click += new System.EventHandler(this.divideButton_Click);//important to use click method so the form works with clicking button

            // 
            // button 4
            // 
            this.fourButton.Location = new System.Drawing.Point(13, 107);
            this.fourButton.Name = "fourButton";
            this.fourButton.Size = new System.Drawing.Size(56, 28);
            this.fourButton.TabIndex = 5;
            this.fourButton.Text = "4";
            this.fourButton.UseVisualStyleBackColor = true;

            this.fourButton.Click += new System.EventHandler(this.fourButton_Click);//important to use click method so the form works with clicking button


            // 
            // button 5
            // 
            this.fiveButton.Location = new System.Drawing.Point(75, 107);
            this.fiveButton.Name = "fiveButton";
            this.fiveButton.Size = new System.Drawing.Size(56, 28);
            this.fiveButton.TabIndex = 6;
            this.fiveButton.Text = "5";
            this.fiveButton.UseVisualStyleBackColor = true;

            this.fiveButton.Click += new System.EventHandler(this.fiveButton_Click);//important to use click method so the form works with clicking button


            // 
            // button 6
            // 
            this.sixButton.Location = new System.Drawing.Point(137, 107);
            this.sixButton.Name = "sixButton";
            this.sixButton.Size = new System.Drawing.Size(56, 28);
            this.sixButton.TabIndex = 7;
            this.sixButton.Text = "6";
            this.sixButton.UseVisualStyleBackColor = true;

            this.sixButton.Click += new System.EventHandler(this.sixButton_Click);//important to use click method so the form works with clicking button

            // 
            // button multiply
            // 
            this.multiplyButton.Location = new System.Drawing.Point(200, 107);
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.Size = new System.Drawing.Size(56, 28);
            this.multiplyButton.TabIndex = 8;
            this.multiplyButton.Text = "*";
            this.multiplyButton.UseVisualStyleBackColor = true;

            this.multiplyButton.Click += new System.EventHandler(this.multiplyButton_Click);//important to use click method so the form works with clicking button


            // 
            // button 1
            // 
            this.oneButton.Location = new System.Drawing.Point(13, 156);
            this.oneButton.Name = "oneButton";
            this.oneButton.Size = new System.Drawing.Size(56, 28);
            this.oneButton.TabIndex = 9;
            this.oneButton.Text = "1";
            this.oneButton.UseVisualStyleBackColor = true;

            this.oneButton.Click += new System.EventHandler(this.oneButton_Click);//important to use click method so the form works with clicking button



            // 
            // button 2
            // 
            this.twoButton.Location = new System.Drawing.Point(75, 156);
            this.twoButton.Name = "twoButton";
            this.twoButton.Size = new System.Drawing.Size(56, 28);
            this.twoButton.TabIndex = 10;
            this.twoButton.Text = "2";
            this.twoButton.UseVisualStyleBackColor = true;

            this.twoButton.Click += new System.EventHandler(this.twoButton_Click);//important to use click method so the form works with clicking button


            // 
            // button 3
            // 
            this.threeButton.Location = new System.Drawing.Point(137, 156);
            this.threeButton.Name = "threeButton";
            this.threeButton.Size = new System.Drawing.Size(56, 28);
            this.threeButton.TabIndex = 11;
            this.threeButton.Text = "3";
            this.threeButton.UseVisualStyleBackColor = true;

            this.threeButton.Click += new System.EventHandler(this.threeButton_Click);//important to use click method so the form works with clicking button


            // 
            // subtract button
            // 
            this.subtractButton.Location = new System.Drawing.Point(200, 156);
            this.subtractButton.Name = "subtractButton";
            this.subtractButton.Size = new System.Drawing.Size(56, 28);
            this.subtractButton.TabIndex = 12;
            this.subtractButton.Text = "-";
            this.subtractButton.UseVisualStyleBackColor = true;

            this.subtractButton.Click += new System.EventHandler(this.subtractButton_Click);//important to use click method so the form works with clicking button

            // 
            // button 0
            // 
            this.zeroButton.Location = new System.Drawing.Point(13, 208);
            this.zeroButton.Name = "zeroButton";
            this.zeroButton.Size = new System.Drawing.Size(56, 28);
            this.zeroButton.TabIndex = 13;
            this.zeroButton.Text = "0";
            this.zeroButton.UseVisualStyleBackColor = true;

            this.zeroButton.Click += new System.EventHandler(this.zeroButton_Click);//important to use click method so the form works with clicking button

            // 
            // decimal button
            // 
            this.decimalButton.Location = new System.Drawing.Point(75, 208);
            this.decimalButton.Name = "decimalButton";
            this.decimalButton.Size = new System.Drawing.Size(56, 28);
            this.decimalButton.TabIndex = 14;
            this.decimalButton.Text = ".";
            this.decimalButton.UseVisualStyleBackColor = true;

            this.decimalButton.Click += new System.EventHandler(this.decimalButton_Click);//important to use click method so the form works with clicking button

            // 
            // Equal Sign Button
            // 
            this.equalButton.Location = new System.Drawing.Point(137, 208);
            this.equalButton.Name = "equalButton";
            this.equalButton.Size = new System.Drawing.Size(56, 28);
            this.equalButton.TabIndex = 15;
            this.equalButton.Text = "=";
            this.equalButton.UseVisualStyleBackColor = true;
            this.equalButton.Click += new System.EventHandler(this.equalButton_Click);
            
            
            // 
            // addition button
            // 
            this.addButton.Location = new System.Drawing.Point(200, 208);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(56, 28);
            this.addButton.TabIndex = 16;
            this.addButton.Text = "+";
            this.addButton.UseVisualStyleBackColor = true;

            this.addButton.Click += new System.EventHandler(this.addButton_Click);//important to use click method so the form works with clicking button



            // 
            // clear button
            // 
            this.clearButton.Location = new System.Drawing.Point(13, 271);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 28);
            this.clearButton.TabIndex = 17;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;

            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);//important to use click method so the form works with clicking button



            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 311);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.equalButton);
            this.Controls.Add(this.decimalButton);
            this.Controls.Add(this.zeroButton);
            this.Controls.Add(this.subtractButton);
            this.Controls.Add(this.threeButton);
            this.Controls.Add(this.twoButton);
            this.Controls.Add(this.oneButton);
            this.Controls.Add(this.multiplyButton);
            this.Controls.Add(this.sixButton);
            this.Controls.Add(this.fiveButton);
            this.Controls.Add(this.fourButton);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.nineButton);
            this.Controls.Add(this.eightButton);
            this.Controls.Add(this.sevenButton);
            this.Controls.Add(this.txtResult);
            this.Name = "Form1";
            this.Text = "Suma's Calculator";
            this.ResumeLayout(false);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;

            this.PerformLayout();

        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        #endregion

        //private System.Windows.Forms.TextBox txtNum1;
        //private System.Windows.Forms.TextBox txtNum2;
        //private KeyPressEventHandler txtNum_KeyPress;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button sevenButton;/// number 7 keyboard 
        private System.Windows.Forms.Button eightButton; /// number 8 keyboard
        private System.Windows.Forms.Button nineButton; /// number 9 keyboard
        private System.Windows.Forms.Button divideButton; /// division button
        private System.Windows.Forms.Button fourButton; /// number 4 keyboard
        private System.Windows.Forms.Button fiveButton; ///number 5 keyboard
        private System.Windows.Forms.Button sixButton; /// number 6 keyboard
        private System.Windows.Forms.Button multiplyButton; /// multiply button
        private System.Windows.Forms.Button oneButton; ///number 1 keyboard
        private System.Windows.Forms.Button twoButton; ///number 2 keyboard
        private System.Windows.Forms.Button threeButton; ///number 3 keyboard
        private System.Windows.Forms.Button subtractButton; ///subtract button
        private System.Windows.Forms.Button zeroButton; ///number 0 keyboard
        private System.Windows.Forms.Button decimalButton; ///decimal point button
        private System.Windows.Forms.Button equalButton; ///equals button
        private System.Windows.Forms.Button addButton; ///addition button
        private System.Windows.Forms.Button clearButton; ///clear button

    }
}