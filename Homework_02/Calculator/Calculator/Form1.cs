﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        double num1 = 0;//first value initialized to 0
        double num2 = 0;//second value initialized to 0


        double result = 0; //totaling up answer

        char operand = ' ';

        public Form1()
        {
            InitializeComponent();
        }


        //button 7
        private void sevenButton_Click(object sender, EventArgs e) {
            if (txtResult.Text==num1.ToString()) {
                txtResult.Text = "7";
            }
            else if (txtResult.Text=="0.")
            {
                txtResult.Text = "7";
            }
            else if (txtResult.Text==result.ToString())
            {
                txtResult.Text = "7";
            }
            else
            {
                txtResult.Text = txtResult.Text + "7";
            }
        }

        //button 8
        private void eightButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "8";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "8";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "8";
            }
            else
            {
                txtResult.Text = txtResult.Text + "8";
            }
        }

        //button 9
        private void nineButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "9";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "9";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "9";
            }
            else
            {
                txtResult.Text = txtResult.Text + "9";
            }
        }

        //divide Button
        private void divideButton_Click(object sender, EventArgs e) {
            num1 = Double.Parse(txtResult.Text);
            operand = '/';
        }

        //button 4
        private void fourButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "4";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "4";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "4";
            }
            else
            {
                txtResult.Text = txtResult.Text + "4";
            }
        }

        //button 5
        private void fiveButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "5";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "5";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "5";
            }
            else
            {
                txtResult.Text = txtResult.Text + "5";
            }
        }

        //button 6
        private void sixButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "6";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "6";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "6";
            }
            else
            {
                txtResult.Text = txtResult.Text + "6";
            }
        }

        //multiply button
        private void multiplyButton_Click(object sender, EventArgs e) {
            num1 = Double.Parse(txtResult.Text);
            operand = '*';
        }

        //button 1
        private void oneButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "1";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "1";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "1";
            }
            else
            {
                txtResult.Text = txtResult.Text + "1";
            }
        }

        //button 2
        private void twoButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "2";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "2";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "2";
            }
            else
            {
                txtResult.Text = txtResult.Text + "2";
            }
        }

        //button 3
        private void threeButton_Click(object sender, EventArgs e) {
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "3";
            }
            else if (txtResult.Text == "0.")
            {
                txtResult.Text = "3";
            }
            else if (txtResult.Text == result.ToString())
            {
                txtResult.Text = "3";
            }
            else
            {
                txtResult.Text = txtResult.Text + "3";
            }
        }

        //subtract button
        private void subtractButton_Click(object sender, EventArgs e) {
            num1 = Double.Parse(txtResult.Text);
            operand = '-';
        }

        //button 0
        private void zeroButton_Click(object sender, EventArgs e) {
          
            if (txtResult.Text == num1.ToString())
            {
                txtResult.Text = "0.";
            }
            else if (txtResult.Text=="0.")
            {
                txtResult.Text = "0.";
            }
            else if (txtResult.Text=="0")
            {
                txtResult.Text = "0.";
            }
            else if (txtResult.Text==num1.ToString())
            {
                txtResult.Text = "0.";
            }
            else
            {
                txtResult.Text = txtResult.Text + "0";
            }
        }

        //decimal Button
        private void decimalButton_Click(object sender, EventArgs e) {
           if (txtResult.Text=="0.")
            {
                txtResult.Text = ".";
            }
           else if (txtResult.Text==result.ToString())
            {
                txtResult.Text = ".";
            }
           else if (txtResult.Text==num1.ToString())
            {
                txtResult.Text = ".";
            }
           else
            {
                if(txtResult.Text.Contains("."))
                {

                }
                else
                {
                    txtResult.Text = txtResult.Text + ".";
                }
            }

        }

        //equal button handler
        private void equalButton_Click(object sender, EventArgs e) {
            num2 = Double.Parse(txtResult.Text);


            switch (operand)
            {
                case '+':
                    result = num1 + num2;
                    break;
                case '-':
                    result = num1 - num2;
                    break;
                case '*':
                    result = num1 * num2;
                    break;
                case '/':
                    result = num1 / num2;
                    break;
            }
            txtResult.Text = result.ToString();
        }



        //addition Button
        private void addButton_Click(object sender, EventArgs e) {
            num1 = Double.Parse(txtResult.Text);
            operand = '+';
        }


        //clear button handler
        private void clearButton_Click(object sender, EventArgs e) {
            num1 = 0;
            num2 = 0;
            result = 0;
            txtResult.Text = "0.";

        }

    }
}
