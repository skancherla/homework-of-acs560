package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	z:= 1.0
	delta := (z*z - x) / (2*z)
	
	//repeat the calculation 10 times, when i used in loop condition i < 10
//and it has exact same value
//i is the number of iterations, like 10 iterations

//	for i:=0; i < 10; i++{
//		z = z - delta;
//		delta = (z*z - x) / (2*z)//don't forget to add those!!
//	}
	
	
	
//this is the second part when I have to change loop condition to stop once value stopped changing
//or only changes by very small delta. How close is math.Sqrt
//using comparison of absolute value of delta if it's greater than very very small number, like 0.00001
	for i := 0; math.Abs(delta) > 0.00001; i++ {
		z = z - delta
		delta = (z*z - x) / (2*z)
	}


	
	return z
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(math.Sqrt(2))
}