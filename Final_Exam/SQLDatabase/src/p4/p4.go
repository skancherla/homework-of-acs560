package main 

import( 
	"fmt"
	"database/sql"
	 _ "github.com/mattn/go-sqlite3" //needed, not the mysql: sqlite1
)
//when i deleted .pb, it will insert two records when i run program, but without deleting .pb file and when i run
//many times, it might have appended two more records

type TestSemesterCourse struct {
	Semester string
	Course string
}

func main() {
	
	//to open a database
	database, err := sql.Open("sqlite3", "p4.db")
	checkErr(err)//failed to create the handle
	if database == nil {
		panic(err)
	}
	
	sql_table := `CREATE TABLE IF NOT EXISTS SemesterCourse(
		Semester VARCHAR(25) NOT NULL,
		Course VARCHAR(100) NOT NULL
	);
	`
	_, err = database.Exec(sql_table) //there may be shouldn't include :=
	checkErr(err)//does create table exist?


	//insert
	stmt, err := database.Prepare("INSERT OR REPLACE INTO SemesterCourse(Semester, Course) values(?, ?)")
	checkErr(err)
	defer stmt.Close()
	
	_, err = stmt.Exec("Fall 2016", "Software Engineering")//don't need colon
	checkErr(err)
	
	_, err = stmt.Exec("Spring 2016", "Software Project Management")//don't need colon
	checkErr(err)
	
	
	//select query
	
	rows, err := database.Query("SELECT * FROM SemesterCourse")
	checkErr(err)
	var semesterSemester string
	var courseCourse string
	
	
	for rows.Next() {
		err = rows.Scan(&semesterSemester, &courseCourse)
		checkErr(err)
		fmt.Println(semesterSemester + " " + courseCourse)
	}
	rows.Close();
	
	database.Close();
	
	
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}