package edu.ipfw.acs560.thecalc;


import java.util.EmptyStackException;

import java.util.Stack;//important to declare stack, not just util.* because there could be errors


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class CalculatorTest {
	
	
	private Stack<Object> s;

    @Before
    public void initializeStack() {
        s = new Stack<>();
    }

    @Test
    public void testEmpty() {
        assertTrue(s.isEmpty());
    }

    //should be EmptyStackException, not StackEmptyException
    @Test (expected = EmptyStackException.class)
    public void testStackEmptyExceptionForPeek() {
    	s.peek();
    }

    @Test (expected = EmptyStackException.class)
    public void testStackEmptyExceptionForPop() {
    	s.pop();
    }


    @Test
    public void testPushAndPeek() {
        s.push("1");
        assertFalse(s.isEmpty());
        assertEquals(s.size(),1);

        Object value = s.peek();
        assertFalse(s.isEmpty());
        assertEquals(s.size(),1);
        
        assertEquals(value,"1");
    }

    @Test
    public void testOnePushAndPop() {
        s.push("1");
        assertFalse(s.isEmpty());
        assertEquals(s.size(),1);

        Object value = s.pop();
        assertTrue(s.isEmpty());
        assertEquals(s.size(),0);

        assertEquals(value,"1");
    }

    @Test
    public void testTwoPushesAndPops() {
        s.push("2");
        assertFalse(s.isEmpty());
        assertEquals(s.size(),1);
        
        s.push("1");
        assertFalse(s.isEmpty());
        assertEquals(s.size(),2);

        Object value1 = s.pop();
        assertFalse(s.isEmpty());
        assertEquals(s.size(),1);
        
        Object value2 = s.pop();
        assertTrue(s.isEmpty());
        assertEquals(s.size(),0);
        
        assertEquals(value1,"1");
        assertEquals(value2,"2");
    }
    
    @Test
    public void testEmptyStack() {
    	String op = " ";
    	try {
    		Calculator.CalculatorFromReversePolish(op);
    	}
    	catch(RuntimeException ex) {
    		//System.out.print(ex);
    	}
    	
    }
    
    @Test
    public void testCharsOnly() {
    	String op = "s";
    	try {
    		Calculator.CalculatorFromReversePolish(op);
    	}
    	catch(RuntimeException ex) {
    		//System.out.print(ex);
    	}
    }
    
    @Test
    public void testIntNoOperator() {
    	String str = "123";
    	try {
    		Calculator.CalculatorFromReversePolish(str);
    		//Integer.parseInt(str);
    	}
    	catch(NumberFormatException ex) {
    		//System.out.print(ex);
    	}
    }
    
    @Test
    public void testCharsPlusOperandOperations() {
    	String blek = "1 2 8 + pj";
    	try {
    		Calculator.CalculatorFromReversePolish(blek);
    	}
    	catch(RuntimeException ex) {
    		//System.out.print(ex);
    	}
    }
    
    @Test
    public void testZero() {
    	int addZeroes = Calculator.CalculatorFromReversePolish("0 0 +");
    	assertEquals(0, addZeroes);
    	
    	int subtractZeroes = Calculator.CalculatorFromReversePolish("0 0 -");
    	assertEquals(0, subtractZeroes);
    	
    	int multZeroes = Calculator.CalculatorFromReversePolish("0 0 *");
    	assertEquals(0, multZeroes);
    	

    }
    
    @Test
    public void testDivideZero() {
    	try {
    		Calculator.CalculatorFromReversePolish("8 0 /");
    	
    	}
    	catch(ArithmeticException ex) {
    		
    	}
    	
    }
    
    
    @Test
    public void testAddition() {
    	int oneAddition = Calculator.CalculatorFromReversePolish("1 1 +");
    	assertEquals(2, oneAddition);
    	
    	int twoAddition = Calculator.CalculatorFromReversePolish("1 2 +");
    	assertEquals(3, twoAddition);
    	
    	int threeAddition = Calculator.CalculatorFromReversePolish("1 3 +");
    	assertEquals(4, threeAddition);
    	
    	int fourAddition = Calculator.CalculatorFromReversePolish("1 4 +");
    	assertEquals(5, fourAddition);
    	
    	int bunchAddition = Calculator.CalculatorFromReversePolish("1 2 3 4 5 8 + + + + +");
    	assertEquals(23, bunchAddition);
    	
    	int humongousAdd = Calculator.CalculatorFromReversePolish("1000 1000 +");
    	assertEquals(2000, humongousAdd);
    	
    	int negationAdd = Calculator.CalculatorFromReversePolish("6 -2 +");
    	assertEquals(4, negationAdd);
    	
    	int nextNegationAdd = Calculator.CalculatorFromReversePolish("-5 -5 +");
    	assertEquals(-10, nextNegationAdd);
    	
    	int thirdNegationAdd = Calculator.CalculatorFromReversePolish("-8 5 +");
    	assertEquals(-3, thirdNegationAdd);
    	
    }
    
    @Test
    public void testSubtraction() {
    	int firstSubtract = Calculator.CalculatorFromReversePolish("1 1 -");
    	assertEquals(0, firstSubtract);
    	
    	int secondSubtract = Calculator.CalculatorFromReversePolish("600 300 -");
    	assertEquals(300, secondSubtract);
    	
    	int negationSubtract = Calculator.CalculatorFromReversePolish("-6 -2 -");
    	assertEquals(-4, negationSubtract);
    	
    	int secondnegationSubtract = Calculator.CalculatorFromReversePolish("-6 2 -");
    	assertEquals(-8, secondnegationSubtract);
    	
    	int thirdnegationSubtract = Calculator.CalculatorFromReversePolish("6 -2 -");
    	assertEquals(8, thirdnegationSubtract);
    }
    
    @Test
    public void testMultiplication() {
    	int firstMultiplication = Calculator.CalculatorFromReversePolish("6 2 *");
    	assertEquals(12, firstMultiplication);
    	
    	int bigNumMult = Calculator.CalculatorFromReversePolish("100 100 *");
    	assertEquals(10000, bigNumMult);
    	
    	int negationMult = Calculator.CalculatorFromReversePolish("-6 -2 *");
    	assertEquals(12, negationMult);
    	
    	int secondnegationMult = Calculator.CalculatorFromReversePolish("-6 2 *");
    	assertEquals(-12, secondnegationMult);
    	
    	int thirdnegationMult = Calculator.CalculatorFromReversePolish("6 -2 *");
    	assertEquals(-12, thirdnegationMult);
    	
    }
    
    @Test
    public void testDivision() {
    	int firstDivision = Calculator.CalculatorFromReversePolish("6 2 /");
    	assertEquals(3, firstDivision);
    	
    	int bigDivision = Calculator.CalculatorFromReversePolish("1000 10 /");
    	assertEquals(100, bigDivision);
    	
    	
    	int negationDiv = Calculator.CalculatorFromReversePolish("-6 -2 /");
    	assertEquals(3, negationDiv);
    	
    	int secondnegationDiv = Calculator.CalculatorFromReversePolish("-6 2 /");
    	assertEquals(-3, secondnegationDiv);
    	
    	int thirdnegationDiv = Calculator.CalculatorFromReversePolish("6 -2 /");
    	assertEquals(-3, thirdnegationDiv);
    }
    
    @Test
    public void testManyOperatives() {
    	
    	//s.push("2");
    	//assertFalse(s.isEmpty());
    	//assertEquals(s.size(), 1);
    	
    	//s.push("1");
    	//assertFalse(s.isEmpty());
    	//assertEquals(s.size(), 2);
    	
    	//int firstPop = Integer.parseInt((String) s.pop());
    	//int secondPop = Integer.parseInt((String)s.pop());
    	//int result = firstPop-secondPop;
    	
    	//assertEquals(result, -1);
    	
    	
    	int heldArray = Calculator.CalculatorFromReversePolish("4 13 5 / +");
    	assertEquals(6, heldArray);
    	
    	int blagg = Calculator.CalculatorFromReversePolish("2 1 + 3 *");
    	assertEquals(9, blagg);
    	
    }
}
