package edu.ipfw.acs560.thecalc;

@SuppressWarnings("serial") //not sure if this is the case
public class Calculator extends RuntimeException {
	public Calculator() {}
	
	public static int CalculatorFromReversePolish(String expression) {
		int result = 0;
		Stack<Integer> stack = new Stack<Integer>();
		String tokens[] = expression.split(" ");//the tokens would be space between
		
		if (tokens.length == 0)
			throw new RuntimeException();//if expression is whitespace and nothing else
		
		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("+")) {
				stack.push(new Integer((Integer)stack.pop() + (Integer)stack.pop()));
			}
			else if (tokens[i].equals("*")) {
				stack.push(new Integer((Integer)stack.pop() * (Integer)stack.pop()));
			}
			else if (tokens[i].equals("-")) {
				//it should be other way around stack values to calculate, otherwise by default it would end up like value1-value2
				int value1=(Integer)stack.pop();
				int value2=(Integer)stack.pop();
				
				//stack.push((Integer)stack.pop() - (Integer)stack.pop());
				
				stack.push(value2-value1);
				
			}
			else if (tokens[i].equals("/")) {
				
				//it should be other way around stack values to calculate, otherwise by default it would end up like value1/value2
				int value1=(Integer)stack.pop();
				int value2=(Integer)stack.pop();
				
				
				//stack.push((Integer)stack.pop() / (Integer)stack.pop());
				stack.push(value2/value1);
			}
			else {
				try {
					Integer op = Integer.parseInt(tokens[i]);
					stack.push(op);
				}
				catch (NumberFormatException e) {
					//System.out.println("contains a string instead of number");
				}
				
			}
		}		
		
		if (stack.isEmpty())
			throw new RuntimeException();
		
		result = stack.pop();
		
		if (!stack.isEmpty())
			throw new RuntimeException();
		
		return result;
	}
}
