package edu.ipfw.acs560.shaper;

/**
 * Implement a Circle, Triangle, and Rectangle class which extend the class Shape. *
 * Ex: public class Circle extends Shape ... etc *
 *
 */

public abstract class Shape {
	
	//no need for abstract constructor though to avoid getting results 0 if happen
	
	public abstract double area();
}