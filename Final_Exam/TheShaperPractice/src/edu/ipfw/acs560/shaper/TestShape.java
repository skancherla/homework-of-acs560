package edu.ipfw.acs560.shaper;

public class TestShape {

	public static void main(String[] args) {
		//easier to put in loop for 100 running results, i is the size

		
		
		for(int i = 0; i <= 100; i++) {
			Shape square = new Square(i);
			Shape circle = new Circle(i);
			Shape rightTriangle = new RightTriangle(i);
			System.out.println("Size: " + i + " square area: " + square.area()
			+ "\tcircle area: " + circle.area() + "\trightTriangle area: " + rightTriangle.area());
		}
		
		
		
		
    }

}
