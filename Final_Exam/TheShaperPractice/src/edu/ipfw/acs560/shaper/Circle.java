package edu.ipfw.acs560.shaper;

public class Circle extends Shape {
	
	private double size;
	
	
	
	public Circle(double size) {
		//super(size); //can't use super constructor, if Shape didn't provide constructor
		this.size=size;
	}
	
	public double area() {
		return (Math.PI * size * size)/4.0;
	}
}