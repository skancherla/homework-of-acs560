package edu.ipfw.acs560.shaper;

public class RightTriangle extends Shape {
	
	private double size;
	
	public RightTriangle(double size) {
		//super(size); //can't use super constructor, if Shape didn't provide constructor
		this.size=size;
	}
	
	public double area() {
		return size * size / 2.0;
	}

}
