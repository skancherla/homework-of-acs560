package edu.ipfw.acs560.blackjack;

public class BlackjackCard extends Card {
	
	private int value;
	
	public BlackjackCard(Rank rank, Suit suit) {
		super(rank, suit);
		
		value = this.getMaxValue();//maxValue by default.., don't comment it out
		
	}
	
	private static int getNonAceValue(Rank rank) {
		
		switch (rank) {
			case Two:
				return 2;
			case Three:
				return 3;
			case Four:
				return 4;
			case Five:
				return 5;
			case Six:
				return 6;
			case Seven:
				return 7;
			case Eight:
				return 8;
			case Nine:
				return 9;
			case Ten: case Jack: case Queen: case King: 
				return 10;
				
		    default:
		    	return 0;
		}
	}
	
	//minimum value of BlackJack card
	public int getMinValue() {
		if (this.isAce()) {
			return 1;
		}
		else
		{
			return getNonAceValue(getRank());
		}
	}
	
	//maximum value of BlackJack card
	public int getMaxValue()
	{
		if (this.isAce()) {
			return 11;
		}
		else {
			return getNonAceValue(getRank());
		}
	}
	
	//the value of the BlackJack card
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	//isAce card
	public boolean isAce() {
		return getRank() == Rank.Ace;	
	}
	
	//isFace card
	public boolean isFace() {
		Rank rank = super.getRank();
		return rank == Rank.Jack ||  rank == Rank.Queen || rank == Rank.King;
	}
}
