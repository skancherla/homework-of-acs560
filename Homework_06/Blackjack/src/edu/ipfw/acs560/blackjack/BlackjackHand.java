package edu.ipfw.acs560.blackjack;

import java.util.Iterator;


public class BlackjackHand extends Hand {
	
	
	//unsure whether handValue is accessing Hand value....
	//but just in case
	
	public void calculationAndAce() {
		//finding all scores or values of Blackjack Hand
		
		int handValue = super.getValue();
				
				
		Iterator<Card> cardIter = cards.iterator();
					
		while (cardIter.hasNext() && handValue > 21) {
			BlackjackCard nextCard = (BlackjackCard) cardIter.next();
			int minValue = nextCard.getMinValue();
			int value = nextCard.getValue();
			
			if (value != minValue) {
				nextCard.setValue(minValue);
				handValue -= value - minValue;//handValue will be the final sum
			}
		}
	}
	
	public void addCard(BlackjackCard blackjackCard) {
		super.addCard(blackjackCard);
		
	}
	
	public boolean isBusted() {
		return getValue() > 21;
	}
	
	public boolean is21() {
		return getValue() == 21;
	}
	
	public boolean isBlackjack() {
		return cards.size() == 2 && is21();
	}
}
