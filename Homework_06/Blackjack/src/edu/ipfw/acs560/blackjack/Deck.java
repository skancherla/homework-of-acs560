package edu.ipfw.acs560.blackjack;


import java.util.ArrayList;

import edu.ipfw.acs560.blackjack.Card.Rank;
import edu.ipfw.acs560.blackjack.Card.Suit;


public class Deck {
	
	private ArrayList<Card> cards;
	
	
	public Deck() {
		
		cards = new ArrayList<Card>();
		
		//for the enumerator for loop, just use the for Rank and Suit class inside
		for (Rank rank : Rank.values()) {
			for (Suit suit : Suit.values()) {
				Card card = new BlackjackCard(rank, suit);//cannot instantiate the class because Card is abstract, but we can use BlackjackCard because of the inheriting
				
				//System.out.println(card.getValue() + " for the value and the rank is "+ card.getRank()+ " and the suit "+card.getSuit());
				
				cards.add(card);
				
				
			}
			
		}
		
	}
	
	public BlackjackHand creation() {
		BlackjackHand hand = new BlackjackHand();
		//create a hand from this deck
		//add a card, using addCard(Card c) void
		
		//this will create a hand after shuffle from first cards on top of deck
		for (int i = 0 ; i < 2; i++) {
			
			hand.addCard(dealCard());
		}

		return hand;
	}
	
	
	
	//shuffle all the cards in this Deck
	public void shuffle() {
		//I chose not to use shuffle collections, but only using random class for the shuffler
		
		
		ArrayList<Card> temp = new ArrayList<Card>(); //create another arraylist for a temp
		while(!cards.isEmpty()) {//while in a loop continuously until deck array is empty, and later will add to another arraylist
			int loc=(int)(Math.random()*cards.size()); //generate random numbers between 52 deck size
			temp.add(cards.get(loc));
			cards.remove(loc);
		}
		cards=temp;//will copy the arraylist after random, to same deck arraylist
		
		
	}
	
	//returning how many remaining cards
	public int getNumberOfCardsRemaining() {
		
		int countAvailable = 0;
		
		for (Card card : cards) {
			if (card.isAvailable()) {
				countAvailable++;
			}
		}
		
		return countAvailable;
	}

	
	//deal a card from this deck
	public Card dealCard() {
		for (Card card : cards) {
		   if (card.isAvailable() ) {
			   card.setAvailable(false); //mark this card as unavailable
		       return card;
		   }
		}
		return null;
	}
	
	

	
	//will print the information later
	

}
