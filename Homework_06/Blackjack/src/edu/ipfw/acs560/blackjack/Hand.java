package edu.ipfw.acs560.blackjack;

import java.util.ArrayList;

import java.util.List;

public class Hand {
	
	protected List<Card> cards = new ArrayList<>();
	
	//add a card to this hand
	public void addCard(Card card)
	{
		cards.add(card);
	}
	
	//calculate the scores of this hand, sum of cards' values
	public int getValue()
	{
			
		int sum = 0;
		for (Card card : cards)
		{
			sum += card.getValue();
			
			
		}
		return sum;
	}
	
	
	//create a hand String message to return list of array to print information in hand
	public String toStringHand() {
		return cards.toString();
	}
	
	
	
}
