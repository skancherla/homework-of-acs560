package edu.ipfw.acs560.blackjack;

public abstract class Card {
	
	private Rank rank;
	private Suit suit;
	private boolean available=true; //by default if don't initialize available, default is false
	
	public enum Rank  {
		
		Ace("A"), Two("2"), Three("3"), Four("4"), Five("5"), Six("6"), Seven("7"), Eight("8"), Nine("9"),
		Ten("10"), Jack("J"), Queen("Q"), King("K");
		
		private String rank;
		
		
		private Rank (String r) {
			this.rank=r;
		}
		
		public String getRank() {
			return rank;
		}
		
	}
	
	
	public enum Suit  {Clubs("c"), Diamonds("d"), Hearts("h"), Spades("s");
		
		private String suit;
		
		private Suit (String s) {
			this.suit=s;
		}
		
		public String getSuit() {
			return suit;
		}
	
	}
	
	
	public Card(Rank rank, Suit suit) {
		
		this.rank=rank;
		this.suit=suit;
	}
	
	public void setSuit(Suit s) {
		this.suit=s;
	}
	
	public void setRank(Rank r) {
		this.rank=r;
	}

	
	public Suit getSuit()
	{
		return suit;
	}
	
	public Rank getRank()
	{
		return rank;
	}
	
	
	
	//is this card available, mark true if available, mark unavailable, set false
	public boolean isAvailable() {
		return available;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	
	//must include those as String to return rank and suit as default rather than just return arraylist in gibberish, this is to print the information
	public String toString() {
		return rank.getRank() + suit.getSuit();
	}
	
	
	//later on to set the mark to available and other to set the mark to unavailable
	
	//value of a Card depend on the game being played
	public abstract int getValue();//it will return int value depending on case
	
}
