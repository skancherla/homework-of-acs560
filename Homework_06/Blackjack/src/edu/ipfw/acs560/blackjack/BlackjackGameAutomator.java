package edu.ipfw.acs560.blackjack;


public class BlackjackGameAutomator {
	private Deck deck;
	private BlackjackHand[] players;
	
	
	
	public void initializeDeck() {
		deck = new Deck();
		deck.shuffle();
	}
	
	public void initialDeal(int numPlayers) {
		players = new BlackjackHand[numPlayers];
		System.out.println(" -- Initial -- ");
		int sum=0;
		for (int i = 0; i < players.length; i++) {
			System.out.print("Hand "+i);
			players[i] = deck.creation();
			sum=players[i].getValue();
			System.out.println(" ("+sum+"): "+players[i].cards);
		}
	}
	
	public void playHand() {
		int sum=0;
		int i=0;
		System.out.println("");

		System.out.println(" -- Completed Game -- ");
		
		for (BlackjackHand player: players) {
			
			System.out.print("Hand "+i);
		//	System.out.println(player.cards);
		//	System.out.println(player.getValue());
			sum=player.getValue();
			
			//I've decided that if sum total is less than 17, will be one more hit, but I can use 4 cards maximum for total
			if(player.getValue()>21) {
				player.isBusted();
				System.out.println(" ("+player.getValue()+"): "+player.cards);
			}
			
			else if(player.getValue()==21) {
				player.is21();
				player.isBlackjack();
				
				System.out.println(" ("+player.getValue()+"): "+player.cards);
			}
			else if(player.getValue() >=17 && player.getValue()<21 ) {
				//System.out.println("You can stop hitting");
				System.out.println(" ("+player.getValue()+"): "+player.cards);
			}
			else {
				player.addCard(deck.dealCard());
				player.calculationAndAce();
				
				//System.out.println(player.cards);
				sum=player.getValue();
				//System.out.println(" ("+sum+"): "+player.cards);
				if(player.getValue()>=17 && player.getValue()<21) {
					//System.out.println("STOP, you're about to get bust on 2nd time");
					System.out.println(" ("+sum+"): "+player.cards);
				}
				else if(player.getValue()==21) {
					player.is21();
					System.out.println(" ("+sum+"): "+player.cards);
				}
				else if (player.getValue()>21) {
					player.isBusted();
					System.out.println(" ("+sum+"): "+player.cards);
				}
				else {
					player.addCard(deck.dealCard());
					player.calculationAndAce();
					//System.out.println(player.cards);
					sum=player.getValue();
					//System.out.println(" ("+sum+"): "+player.cards);
					if(player.getValue()>21) {
						player.isBusted();
						System.out.println(" ("+sum+"): "+player.cards);
					}
					else if(player.getValue()==21) {
						player.is21();
						System.out.println(" ("+sum+"): "+player.cards);
					}
					else {
						System.out.println(" ("+sum+"): "+player.cards);
					}
				}	
			}
			i++;
		}
	}
	
	
	

	
	
	//find the winner with what number value
	public int winner() {
		int max=0;
		int i;
		for(i=0; i < players.length; i++) {
			if(players[i].is21() ||players[i].isBlackjack()||(!players[i].isBusted())) {
				
				if(players[i].getValue()>=max) {
				//	if(players[i].getValue()==21&&players[i].isBlackjack()) {
				//		max=players[i].getValue();
				//	}
					max=players[i].getValue();
				}
				
			}
			
		}
		return max;
	}
	
	//prints the hand info on who's the winner with best blackjack or winner score
	public void printWinner() {
		System.out.print("Winners: ");
		
		for(int i=0; i< players.length; i++) {
			if(winner()==players[i].getValue()) {
				System.out.print(i+", ");
			}
		}
		
	}
	
	
	
	
	
	public void playAllHands() {
		for(int i=0; i < 6; i++) {
			playHand();
		}

	}
}
