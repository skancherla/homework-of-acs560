package edu.ipfw.acs560.blackjack;


public class BlackjackTester {

	public static void main(String[] args) throws Exception {
				
		BlackjackGameAutomator g = new BlackjackGameAutomator();
		g.initializeDeck();
		g.initialDeal(5);
		g.playHand();
		g.winner();
		g.printWinner();
		
		


	}

}
