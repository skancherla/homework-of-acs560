package main


import (
	"io"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "<!DOCTYPE html>\n")
    io.WriteString(w, "<html lang=\"en\">\n")
	io.WriteString(w, "<head>\n")
	io.WriteString(w, "<meta charset=\"utf-8\">\n");	
	io.WriteString(w, "<title>Hello ACS560!</title>\n")
	io.WriteString(w, "</head>\n")
	io.WriteString(w, "<body>\n")
	io.WriteString(w, "<h2>Hello ACS560!</h2>\n")
	io.WriteString(w, "</body>\n")
	io.WriteString(w, "</html>\n")
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":12000", nil)
}


