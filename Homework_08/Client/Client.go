package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	
	if len(os.Args) < 2 {
		log.Fatal("URL parameter required")
	}
	
	url := os.Args[1]

	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	
	robots, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	
	if err != nil {
		log.Fatal(err)
	}
	
	fmt.Printf("%s", robots)
}