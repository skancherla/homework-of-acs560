package main 

import (
	"bufio"
	"html/template"
	"os"
)

type Course struct
{
	Term string
	Title string
}

func main() {
	courses := []Course {
		
		Course {
			Term: "Spring 2015",
			Title: "Software Project Management",
		},
		Course {
			Term: "Spring 2015",
			Title: "Know Discovery & Data Mining",
		},
		Course {
			Term: "Summer 2015",
			Title: "Compiler Construction",
		},
		Course {
		    Term: "Fall 2015",
			Title: "Embedded Systems",
		},
		Course {
		    Term: "Fall 2015",
			Title: "Advanced Computer Networking",
		},
		Course {
		    Term: "Spring 2016",
			Title: "Cryptography and Network Security",
		},
		Course {
		    Term: "Spring 2016",
			Title: "Machine Learning",
		},
		Course {
		    Term: "Fall 2016",
			Title: "Software Engineering",
		},
		Course {
		    Term: "Fall 2016",
			Title: "Internet of Things",
		},
	}
	
	t, err := template.ParseFiles("htdocs/hw3.tmpl")//;
	if err != nil { panic(err) }
	
	file, err := os.Create("htdocs/hw.html")
	if err != nil { panic(err) }
	
	writer := bufio.NewWriter(file)
	
	t.Execute(writer, courses)
	writer.Flush()
}

