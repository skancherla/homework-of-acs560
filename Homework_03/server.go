package main

import  (
	"io/ioutil"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {     //pointer is the request
    path := r.URL.Path
    
    if path == "/" {
	    path = "/index.html"
    }    
    
    data, err := ioutil.ReadFile("htdocs" + path)
    
    if err == nil {
		w.Write(data)
    } else {
    	http.NotFound(w,r)
    }
}

func mainn() {
	http.HandleFunc("/", hello);
	http.ListenAndServe(":8080", nil)
}
