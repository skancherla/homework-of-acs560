﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;

namespace hw3 {
    struct studentRecord {
          public String classTerm, className;
    }

    class Program
    {

        static void Main(string[] args) {

            StreamWriter sWrite = new StreamWriter("studentRecords.html");


            sWrite.WriteLine("<html>");
            sWrite.WriteLine("<head>");
            sWrite.WriteLine("\t<title>My Courses</title>");
            sWrite.WriteLine("</head>");
            sWrite.WriteLine("<body>");
            sWrite.WriteLine("\t<table style=\"font-family: times new roman; border: 2px solid black\">");
            sWrite.WriteLine("<tr>");
            sWrite.WriteLine("<th style=\"text-align: center; border: 1px solid black; padding: 5px;\">Semester</th>");
            sWrite.WriteLine("<th style=\"text-align: center; border: 1px solid black; padding: 5px;\">Course</th>");
            sWrite.WriteLine("</tr>");


            studentRecord[] arr = new studentRecord[9];

            arr[0].classTerm = "Spring 2015";
            arr[0].className = "Know Discovery and Data Mining";
            arr[1].classTerm = "Spring 2015";
            arr[1].className = "Software Project Management";
            arr[2].classTerm = "Summer 2015";
            arr[2].className = "Compiler Construction";
            arr[3].classTerm = "Fall 2015";
            arr[3].className = "Embedded Systems";
            arr[4].classTerm = "Fall 2015";
            arr[4].className = "Advanced Computer Networking";
            arr[5].classTerm = "Spring 2016";
            arr[5].className = "Cryptography and Network Security";
            arr[6].classTerm = "Spring 2016";
            arr[6].className = "Machine Learning";
            arr[7].classTerm = "Fall 2016";
            arr[7].className = "Software Engineering";
            arr[8].classTerm = "Fall 2016";
            arr[8].className = "Internet of Things";


            for (int i = 0; i < arr.Length; i++)
            {
                sWrite.WriteLine("<tr>");
                sWrite.WriteLine("<td style=\"border: 1px solid black; padding: 5px\">"+arr[i].classTerm+"</td>");
                sWrite.WriteLine("<td style=\"border: 1px solid black; padding: 5px\">" + arr[i].className + "</td>");
                sWrite.WriteLine("</tr>");
            }
            sWrite.WriteLine("</table>");
            sWrite.WriteLine("</body>");
            sWrite.WriteLine("</html>");
            sWrite.Close();
        }

    }
}