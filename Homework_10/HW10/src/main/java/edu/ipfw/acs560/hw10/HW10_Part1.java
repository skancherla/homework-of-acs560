/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ipfw.acs560.hw10;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.sqlite.SQLiteDataSource;

/**
 *
 * @author User
 */
public class HW10_Part1 {
    
    private static final String FIND_PROFS_AVGS = 
        "select ProfessorID, avg(evaluation) as ProfessorAvg\n" +
        "from CourseEvaluation group by ProfessorID\n";
    
    private static final String FIND_PROFS_ABOVE_AVG =
        "select ProfessorID, avg(evaluation) as ProfessorAvg\n" +
        "from CourseEvaluation group by ProfessorID\n" +
        "having avg(evaluation) >\n" +
        "(select avg(ProfessorAvg) from\n" +
        "(select ProfessorID, avg(evaluation) as ProfessorAvg\n" +
        "from CourseEvaluation group by ProfessorID))\n";
    
    public static void main(String[] args) throws SQLException {
        
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl("jdbc:sqlite:p1.db");
        try (Connection connection = dataSource.getConnection())
        {
            PreparedStatement findProfsAvgs
                    = connection.prepareStatement(FIND_PROFS_AVGS);
            
            ResultSet profsAvgsResults = findProfsAvgs.executeQuery();
            
            System.out.println("All Professor Averages");
            while (profsAvgsResults.next())
            {
                int profId = profsAvgsResults.getInt("ProfessorID");
                double profAvg = profsAvgsResults.getDouble("ProfessorAvg");
                
                System.out.println("ProfessorID: " + profId + 
                                   "  Avg Rating: " + profAvg);
            }
            
            PreparedStatement findProfsAboveAvg
                    = connection.prepareStatement(FIND_PROFS_ABOVE_AVG);
            
            ResultSet profsAboveAvgResults = findProfsAboveAvg.executeQuery();
            
            System.out.println("Above Average Professor Averages");
            while (profsAboveAvgResults.next())
            {
                int profId = profsAboveAvgResults.getInt("ProfessorID");
                double profAvg = profsAboveAvgResults.getDouble("ProfessorAvg");
                
                System.out.println("ProfessorID: " + profId + 
                                   "  Avg Rating: " + profAvg);
            }
        }
    }
}
