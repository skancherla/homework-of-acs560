/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ipfw.acs560.hw10;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.sqlite.SQLiteDataSource;

/**
 *
 * @author User
 */
public class HW10_Part2 {
    
    private static final String CREATE_SEMESTER_COURSE = 
        "create table if not exists SemesterCourse(Semester text, Course text)";
    
    private static final String SELECT_SEMESTER_COURSE =
        "select Semester, Course from SemesterCourse";
    
    private static final String INSERT_SEMESTER_COURSE =
        "insert into SemesterCourse (Semester, Course) values (?,?)";
    
    public static void main(String[] args) throws SQLException {
        String semester="";
        String course="";
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl("jdbc:sqlite:p2.db");
        
        //I added
        System.out.println("Opened database successfully");
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement createTable = 
                connection.prepareStatement(CREATE_SEMESTER_COURSE);
            
            createTable.execute();
            
            PreparedStatement selectSemesterCourse = 
                connection.prepareStatement(SELECT_SEMESTER_COURSE);
            
            ResultSet semesterCourseResults =
                selectSemesterCourse.executeQuery();
            
            if (!semesterCourseResults.next()) {
                PreparedStatement insertSemesterCourse =
                    connection.prepareStatement(INSERT_SEMESTER_COURSE);
                
                insertSemesterCourse.setString(1, "Fall 2016");
                insertSemesterCourse.setString(2, "Software Engineering");
                insertSemesterCourse.execute();
                
                
                insertSemesterCourse.setString(1, "Spring 2016");
                insertSemesterCourse.setString(2, "Software Project Management");
                insertSemesterCourse.execute();
                
            }//end if statement
            
            
                semester = semesterCourseResults.getString("Semester");
                course = semesterCourseResults.getString("Course");
                
                System.out.println("Semester = " + semester);
                System.out.println("Course = " + course);
                
            //while statement starts out with next row instead of first row
            while (semesterCourseResults.next()) {
                semester = semesterCourseResults.getString("Semester");
                course = semesterCourseResults.getString("Course");
                
                System.out.println("Semester = " + semester);
                System.out.println("Course = " + course);
            }//end while statement
            semesterCourseResults.close();
            selectSemesterCourse.close();
            connection.close();
        }//end try
        System.out.println("Operation done successfully");
    }
}
